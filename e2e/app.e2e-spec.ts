import { AmbulancePage } from './app.po';

describe('ambulance App', () => {
  let page: AmbulancePage;

  beforeEach(() => {
    page = new AmbulancePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
